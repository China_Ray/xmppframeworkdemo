//
//  LoginViewController.m
//  XMPPFrameworkTest
//
//  Created by Admin on 15/8/10.
//  Copyright (c) 2015年 test. All rights reserved.
//

#import "LoginViewController.h"
#import "BuddyListViewController.h"
#import "XMPPManager.h"
#import "UserManager.h"

@interface LoginViewController () <UITextFieldDelegate,UIAlertViewDelegate,XMPPConnectDelegate>

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setTitle:@"登录"];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *directory = [paths objectAtIndex:0];
    
    NSString *dbPath = [directory stringByAppendingPathComponent:[[[NSBundle mainBundle] bundleIdentifier] stringByAppendingString:@".cache"]];
    [[NSFileManager defaultManager] createDirectoryAtPath:dbPath withIntermediateDirectories:YES attributes:nil error:nil];
    NSLog(@"dbpath is %@",dbPath);

    [self.nameTextField         setText:[[UserManager manager] userLoginName]];
    [self.passwordTextField     setText:[[UserManager manager] userLoginPassword]];
    [self.nameTextField     setDelegate:self];
    [self.passwordTextField setDelegate:self];
    [self.serverTextField   setDelegate:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//login
- (IBAction)loginButtonClick:(UIButton *)sender {
    
    [self.nameTextField     resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.serverTextField   resignFirstResponder];
    
    if ([self validateWithUser:self.nameTextField.text andPass:self.passwordTextField.text addServer:self.serverTextField.text]) {
        [[XMPPManager manager] loginWithJID:self.nameTextField.text password:self.passwordTextField.text server:self.serverTextField.text];
        [[XMPPManager manager] setConnectDelegate:self];
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入用户名，密码和服务器" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)registerButtonClick:(UIButton *)sender {
    
    NSLog(@"register");
    [self.nameTextField     resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.serverTextField   resignFirstResponder];
    
    if ([self validateWithUser:self.nameTextField.text andPass:self.passwordTextField.text addServer:self.serverTextField.text]) {
        [[XMPPManager manager] registerWithJID:self.nameTextField.text password:self.passwordTextField.text server:self.serverTextField.text];
        [[XMPPManager manager] setConnectDelegate:self];
    }else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请输入用户名，密码和服务器" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(BOOL)validateWithUser:(NSString *)userText andPass:(NSString *)passText addServer:(NSString *)serverText{
    
    if (userText.length > 0 && passText.length > 0 && serverText.length > 0) {
        return YES;
    }
    return NO;
}

#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        [self gotoBuddyListVC];
    }
}

#pragma mark - XMPPConnect Delegate

- (void)connectCompletion
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"友情提示" message:@"登录成功，欢迎回来" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView setTag:1];
    [alertView show];
    
    [[UserManager manager] saveWithName:self.nameTextField.text Password:self.passwordTextField.text Server:self.serverTextField.text];
}

- (void)connectFailure
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"友情提示" message:@"登录失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView setTag:0];
    [alertView show];
}

- (void)registerCompletion
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"友情提示" message:@"注册成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView setTag:1];
    [alertView show];
    
    [[UserManager manager] saveWithName:self.nameTextField.text Password:self.passwordTextField.text Server:self.serverTextField.text];
}

- (void)registerFailure
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"友情提示" message:@"注册失败" delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alertView setTag:0];
    [alertView show];
}

#pragma marl - Model Segue

- (void)gotoBuddyListVC
{
    BuddyListViewController *viewController = [[BuddyListViewController alloc] init];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    [self presentViewController:navigationController animated:YES completion:^{
        [[XMPPManager manager] setConnectDelegate:nil];
    }];
}

@end
