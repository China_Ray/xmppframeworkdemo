//
//  BuddyListViewController.m
//  XMPPFrameworkTest
//
//  Created by Admin on 15/8/10.
//  Copyright (c) 2015年 test. All rights reserved.
//

#import "BuddyListViewController.h"
#import "XMPPManager.h"
#import "UserManager.h"

@interface BuddyListViewController () <UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *onlineUsers;

@end

@implementation BuddyListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setTitle:@"好友列表"];
    [self setNavigationBar];
    
    [self.tableView setDataSource:self];
    [self.tableView setDelegate:self];
    self.onlineUsers = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Main

- (void)setNavigationBar
{
    UIButton *refresh = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44.0f, 44.0f)];
    [refresh setBackgroundColor:[UIColor grayColor]];
    [refresh setTitle:@"刷新" forState:UIControlStateNormal];
    [refresh setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [refresh addTarget:self action:@selector(refreshButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *refreshBar = [[UIBarButtonItem alloc] initWithCustomView:refresh];
    [refreshBar setStyle:UIBarButtonItemStyleDone];
    [self.navigationItem setRightBarButtonItem:refreshBar];
    
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44.0f, 44.0f)];
    [back setBackgroundColor:[UIColor grayColor]];
    [back setTitle:@"返回" forState:UIControlStateNormal];
    [back setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBar = [[UIBarButtonItem alloc] initWithCustomView:back];
    [backBar setStyle:UIBarButtonItemStyleDone];
    [self.navigationItem setLeftBarButtonItem:backBar];
}

#pragma mark - UITableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.onlineUsers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"userCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    return cell;
}

#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

#pragma mark - UIResponse Event

//刷新按钮
- (void)refreshButtonClick:(id)sender
{
    NSArray *rosterList = [[XMPPManager manager] rosterList];
    NSLog(@"roster list is %@",rosterList);
}

//返回按钮
- (void)backButtonClick:(id)sender
{
    [[XMPPManager manager] disconnect];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
