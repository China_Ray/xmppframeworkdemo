//
//  BuddyListViewController.h
//  XMPPFrameworkTest
//
//  Created by Admin on 15/8/10.
//  Copyright (c) 2015年 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuddyListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

