//
//  LoginViewController.h
//  XMPPFrameworkTest
//
//  Created by Admin on 15/8/10.
//  Copyright (c) 2015年 test. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *serverTextField;

//login
- (IBAction)loginButtonClick:(UIButton *)sender;
//register
- (IBAction)registerButtonClick:(UIButton *)sender;

@end
