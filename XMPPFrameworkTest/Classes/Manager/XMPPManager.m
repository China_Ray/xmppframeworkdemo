//
//  XMPPManager.m
//  XMPPFrameworkTest
//
//  Created by Admin on 15/8/10.
//  Copyright (c) 2015年 test. All rights reserved.
//

#import "XMPPManager.h"
#import "XMPPFramework.h"

@interface XMPPManager () <XMPPStreamDelegate,XMPPReconnectDelegate,XMPPAutoPingDelegate,XMPPRosterDelegate>
{
    //xmpp基础服务类
    XMPPStream *xmppStream;
    //xmpp自动重连
    XMPPReconnect *xmppReconnect;
}

//服务器名
@property (copy, nonatomic  ) NSString                            *xmppHost;
//主机
@property (copy, nonatomic  ) NSString                            *xmppServer;
//是否需要注册
@property (assign           ) BOOL                                xmppNeedRegister;
//密码
@property (copy, nonatomic  ) NSString                            *myPassword;

//心跳
@property (strong, nonatomic) XMPPAutoPing                        *xmppAutoPing;

//花名册
@property (strong, nonatomic) XMPPRosterCoreDataStorage           *xmppRosterStorage;
@property (strong, nonatomic) XMPPRoster                          *xmppRoster;

//消息
@property (strong, nonatomic) XMPPMessageArchivingCoreDataStorage *xmppMessageStorage;
@property (strong, nonatomic) XMPPMessageArchiving                *xmppMessage;

@end

@implementation XMPPManager
@synthesize xmppHost,xmppServer,xmppNeedRegister,myPassword,xmppAutoPing;
@synthesize xmppRosterStorage,xmppRoster;
@synthesize xmppMessageStorage,xmppMessage;

+ (instancetype)manager
{
    static XMPPManager *manager = nil;
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        manager = [[self alloc] init];
        manager->xmppHost = @"altamob";
        manager->xmppServer = @"10.1.7.211";
        manager->xmppNeedRegister = NO;
    });
    return manager;
}

//static dispatch_queue_t xmpp_creation_queue() {
//    static dispatch_queue_t creation_queue;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        creation_queue = dispatch_queue_create("com.altamob.xmpp.queue.creation", DISPATCH_QUEUE_CONCURRENT);
//    });
//    
//    return creation_queue;
//}

#pragma mark - 基本设置 Method

//初始化xmpp
- (void)setupStream
{
    if (xmppStream != nil) {
        return ;
    }

    xmppStream    = [[XMPPStream alloc] init];
    [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];

    xmppReconnect = [[XMPPReconnect alloc] init];
    [xmppReconnect setAutoReconnect:YES];
    [xmppReconnect activate:xmppStream];
    [xmppReconnect addDelegate:self delegateQueue:dispatch_get_main_queue()];

    xmppAutoPing  = [[XMPPAutoPing alloc] init];
    [xmppAutoPing setRespondsToQueries:YES];
    [xmppAutoPing setPingInterval:2];
    [xmppAutoPing activate:xmppStream];
    [xmppAutoPing addDelegate:self delegateQueue:dispatch_get_main_queue()];
}

//停用xmpp
- (void)uninstallStream
{
    //
    [xmppReconnect deactivate];
    [xmppReconnect removeDelegate:self];
    xmppReconnect = nil;
    //
    [xmppAutoPing deactivate];
    [xmppAutoPing removeDelegate:self];
    xmppAutoPing  = nil;
    //
    [xmppStream disconnect];
    [xmppStream removeDelegate:self];
    xmppStream    = nil;
}

//激活相关的模块
- (void)activeModules
{
    //花名册
    xmppRosterStorage  = [XMPPRosterCoreDataStorage sharedInstance];
    //xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] init];
    xmppRoster         = [[XMPPRoster alloc] initWithRosterStorage:xmppRosterStorage];
    //自动同步，从服务器取出好友
    [xmppRoster setAutoFetchRoster:YES];
    //关掉自动接收好友请求，默认开启自动同意
    [xmppRoster setAutoAcceptKnownPresenceSubscriptionRequests:NO];
    [xmppRoster activate:xmppStream];
    [xmppRoster addDelegate:self delegateQueue:dispatch_get_main_queue()];

    //消息
    xmppMessageStorage = [XMPPMessageArchivingCoreDataStorage sharedInstance];
    //xmppMessageStorage = [[XMPPMessageArchivingCoreDataStorage alloc] init];
    xmppMessage        = [[XMPPMessageArchiving alloc] initWithMessageArchivingStorage:xmppMessageStorage];
    [xmppMessage activate:xmppStream];
    [xmppMessage addDelegate:self delegateQueue:dispatch_get_main_queue()];
}

//登录
- (BOOL)loginWithJID:(NSString *)myJID password:(NSString *)password server:(NSString *)server
{
    if ([xmppStream isAuthenticated]) {
        return YES;
    }
    
    if ([xmppStream isConnected]) {
        [self disconnect];
    }

    self.myPassword = password;
    xmppNeedRegister = NO;

    XMPPJID *myXmppJID = [XMPPJID jidWithString:[NSString stringWithFormat:@"%@@%@",myJID,xmppHost]];
    [xmppStream setMyJID:myXmppJID];
    [xmppStream setHostName:server];
    
    NSError *error = nil;
    BOOL connectStatus = [xmppStream connectWithTimeout:60 error:&error];
    if (!connectStatus) {
        NSLog(@"connect error is %@",error.localizedDescription);
    }
    return connectStatus;
}

//注册
- (BOOL)registerWithJID:(NSString *)myJID password:(NSString *)password server:(NSString *)server
{
    if ([xmppStream isAuthenticated]) {
        return YES;
    }

    if ([xmppStream isConnected]) {
        [self disconnect];
    }

    self.myPassword    = password;
    xmppNeedRegister   = YES;

    XMPPJID *myXmppJID = [XMPPJID jidWithString:[NSString stringWithFormat:@"%@@%@",myJID,xmppHost]];
    [xmppStream setMyJID:myXmppJID];
    [xmppStream setHostName:server];

    NSError *error     = nil;
    BOOL connectStatus = [xmppStream connectWithTimeout:60 error:&error];
    if (!connectStatus) {
        NSLog(@"connect error is %@",error.localizedDescription);
    }
    return connectStatus;
}

//断开连接
- (void)disconnect
{
    [self offline];
    [xmppStream disconnect];
}

//上线
- (void)online
{
    XMPPPresence *presence = [XMPPPresence presence];
    [xmppStream sendElement:presence];
}

//下线
- (void)offline
{
    XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
    [xmppStream sendElement:presence];
}

#pragma mark - 基本设置 Delegate

//连接超时
- (void)xmppStreamConnectDidTimeout:(XMPPStream *)sender
{
    NSLog(@"Connect Timeout");
}

//身份认证
- (void)xmppStreamDidConnect:(XMPPStream *)sender
{
    if (xmppNeedRegister) {
        //注册方法
        NSError *error = nil;
        BOOL status = [xmppStream registerWithPassword:self.myPassword error:&error];
        NSLog(@"status is %d, error is %@",status,error);
    } else {
        //身份认证方法
        [xmppStream authenticateWithPassword:self.myPassword error:nil];
    }
}

//xmpp stream失败
- (void)xmppStream:(XMPPStream *)sender didReceiveError:(DDXMLElement *)error
{
    NSLog(@"didReceiveError error is %@",[error description]);
}

//身份认证通过
- (void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{
    NSLog(@"验证通过");
    [self online];
    //激活相关模块
    [self activeModules];
    
    [self.connectDelegate connectCompletion];
}

//身份认证失败
- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(DDXMLElement *)error
{
    NSLog(@"验证失败 error is %@",[error description]);
    [self.connectDelegate connectFailure];
}

//注册通过
- (void)xmppStreamDidRegister:(XMPPStream *)sender
{
    NSLog(@"注册成功");
    [self online];
    //激活相关模块
    [self activeModules];
    
    [self.connectDelegate registerCompletion];
}

//注册失败
- (void)xmppStream:(XMPPStream *)sender didNotRegister:(DDXMLElement *)error
{
    NSLog(@"注册失败 error is %@",error.description);
    [self.connectDelegate registerFailure];
}

//失去连接，掉线、断网故障时不执行
- (void)xmppStreamWasToldToDisconnect:(XMPPStream *)sender
{
    
}

//失去连接，掉线、断网故障时执行
- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error
{
    //[error localizedDescription]
    NSLog(@"服务器连接失败 error is %@",error);
    //[self.connectDelegate connectFailure];
}

#pragma mark - 心跳监听 Delegate

//发送
- (void)xmppAutoPingDidSendPing:(XMPPAutoPing *)sender
{
    //NSLog(@"AutoPingDidSendPing");
}

//接收
- (void)xmppAutoPingDidReceivePong:(XMPPAutoPing *)sender
{
    //NSLog(@"AutoPingDidReceivePong");
}

//超时
- (void)xmppAutoPingDidTimeout:(XMPPAutoPing *)sender
{
    NSLog(@"AutoPingDidTimeout");
}

#pragma mark - 花名册 Method

//获取好友列表
- (NSArray *)rosterList
{
    [xmppRoster fetchRoster];
    return nil;
    
//    NSManagedObjectContext *context = [xmppRosterStorage mainThreadManagedObjectContext];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:NSStringFromClass([XMPPUserCoreDataStorageObject class]) inManagedObjectContext:context];
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    [request setEntity:entity];
//    [request setFetchBatchSize:10];
//
//    //在线状态排序
//    NSSortDescriptor *statusSort = [NSSortDescriptor sortDescriptorWithKey:@"sectionNum" ascending:YES];
//    //显示名称排序
//    NSSortDescriptor *nameSort = [NSSortDescriptor sortDescriptorWithKey:@"displayName" ascending:YES];
//    [request setSortDescriptors:@[statusSort,nameSort]];
//    
//    NSError *error = nil;
//    NSArray *rosterList = [context executeFetchRequest:request error:&error];
//    if (error == nil) {
//        return rosterList;
//    } else {
//        NSLog(@"toster list is error %@",[error localizedDescription]);
//        return nil;
//    }
}

//添加好友
- (void)addBuddy:(NSString *)name
{
    XMPPJID *jid = [XMPPJID jidWithString:[NSString stringWithFormat:@"%@@%@",name,xmppHost]];
    [xmppRoster subscribePresenceToUser:jid];
}

//删除好友
- (void)removeBuddy:(NSString *)name
{
    XMPPJID *jid = [XMPPJID jidWithString:[NSString stringWithFormat:@"%@@%@",name,xmppHost]];
    [xmppRoster removeUser:jid];
}

#pragma mark - 花名册 Delegate

//获取添加好友请求
- (void)xmppRoster:(XMPPRoster *)sender didReceivePresenceSubscriptionRequest:(XMPPPresence *)presence
{
    //好友状态
    NSString *presenceType     = [presence type];
    //好友名称
    NSString *presenceFromUser = [[presence from] user];
    
    NSLog(@"didReceivePresenceSubscriptionRequest status is %@, name is %@",presenceType, presenceFromUser);
    //    //通过
    //    XMPPJID *jid = [XMPPJID jidWithString:presenceFromUser];
    //    [xmppRoster acceptPresenceSubscriptionRequestFrom:jid andAddToRoster:YES];
}

//接收好友状态
- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    NSLog(@"didReceivePresence is %@",presence);
    
    //获取好友状态
    NSString *presenceType     = [presence type];
    //收到对方取消定阅我得消息
    if ([presenceType isEqualToString:@"unsubscribe"]) {
        [self.xmppRoster removeUser:presence.from];
    }

    //当前用户
    NSString *userID           = [[sender myJID] user];
    //请求的用户
    NSString *presenceFromUser = [[presence from] user];
    
    if (![presenceFromUser isEqualToString:userID]) {
        if ([presenceType isEqualToString:@"available"]) {
            //在线状态
            //用户列表委托
            //[self.chatDelegate buddyOnline:presenceFromUser];
        } else if ([presenceType isEqualToString:@"unavailable"]) {
            //离线状态
            //用户列表委托
            //[self.chatDelegate buddyOffline:presenceFromUser];
        } else if ([presenceType isEqualToString:@"away"]) {
            //离开状态
        } else if ([presenceType isEqualToString:@"do not disturb"]) {
            //忙碌状态
        } else {
            //其他状态
        }
    }
}

//开始同步服务器发送过来的自己的好友列表
- (void)xmppRosterDidBeginPopulating:(XMPPRoster *)sender withVersion:(NSString *)version
{

}

//获取单个好友节点
- (void)xmppRoster:(XMPPRoster *)sender didReceiveRosterItem:(DDXMLElement *)item
{
    NSLog(@"didReceiveRosterItem is %@",[item description]);
}

//同步结束，收到好友列表IQ会进入的方法，并且已经存入我的存储器
- (void)xmppRosterDidEndPopulating:(XMPPRoster *)sender
{
    NSLog(@"RosterDidEndPopulating is %@",[sender description]);
}

- (void)xmppStream:(XMPPStream *)sender didSendIQ:(XMPPIQ *)iq
{
    NSLog(@"didSendIQ is %@",iq);
}

- (void)xmppStream:(XMPPStream *)sender didFailToSendIQ:(XMPPIQ *)iq error:(NSError *)error
{
    NSLog(@"didFailToSendIQ is %@, error is %@",iq,error);
}

#pragma mark - 收发消息 Method

//发送消息
- (void)sendMessage:(NSString *)message ToUser:(NSString *)user
{
    NSXMLElement *body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:message];
    
    NSXMLElement *mess = [NSXMLElement elementWithName:@"message"];
    [mess addAttributeWithName:@"from" stringValue:xmppStream.myJID.bare];
    [mess addAttributeWithName:@"to" stringValue:[NSString stringWithFormat:@"%@@%@",user,xmppHost]];
    [mess addAttributeWithName:@"type" stringValue:@"chat"];
    [mess addChild:body];
    
    //发送
    [xmppStream sendElement:mess];
}

#pragma mark - 收发消息 Delegate

//接收消息
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    NSLog(@"didReceiveMessage is %@",message);
    NSString *msg  = [[message elementForName:@"body"] stringValue];
    NSString *from = [[message attributeForName:@"from"] stringValue];

    if (msg != nil && ![msg isKindOfClass:[NSNull class]]) {
        NSLog(@"message from is %@, msg is %@",from,msg);
    }
//
//    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//    [dict setObject:msg forKey:@"msg"];
//    [dict setObject:from forKey:@"from"];
//    
//    //消息委托。
//    [self.messageDelegate newMessaheReceived:dict];
}

//发送消息
- (void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message
{
    NSString *msg = [[message elementForName:@"body"] stringValue];
    NSString *to  = [[message attributeForName:@"to"] stringValue];

    if (msg != nil && ![msg isKindOfClass:[NSNull class]]) {
        NSLog(@"message to is %@, msg is %@",to,msg);
    }
}

//消息发送失败
- (void)xmppStream:(XMPPStream *)sender didFailToSendMessage:(XMPPMessage *)message error:(NSError *)error
{
    NSLog(@"消息发送失败");
}

#pragma mark - XMPPReconect Delegate

@end
