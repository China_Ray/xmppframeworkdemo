//
//  UserManager.h
//  XMPPFrameworkTest
//  功能描述 - 用户管理器
//  Created by Admin on 15/8/11.
//  Copyright (c) 2015年 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserManager : NSObject

//用户名
@property (copy, nonatomic) NSString *userName;
//密码
@property (copy, nonatomic) NSString *userPassword;
//服务器地址
@property (copy, nonatomic) NSString *serverName;

+ (instancetype)manager;

//获取用户登录名
- (NSString *)userLoginName;
//获取用户密码
- (NSString *)userLoginPassword;
//获取用户登录地址
- (NSString *)userLoginServer;

//保存用户信息
- (void)saveWithName:(NSString *)_name
            Password:(NSString *)_password
              Server:(NSString *)_server;

@end
