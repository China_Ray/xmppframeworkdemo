//
//  XMPPManager.h
//  XMPPFrameworkTest
//  功能描述 - xmpp管理器
//  Created by Admin on 15/8/10.
//  Copyright (c) 2015年 test. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol XMPPConnectDelegate <NSObject>
//连接成功
- (void)connectCompletion;
//连接失败
- (void)connectFailure;
//注册成功
- (void)registerCompletion;
//注册失败
- (void)registerFailure;
@end

@protocol XMPPChatDelegate <NSObject>
//好友上线
- (void)buddyOnline:(NSString *)buddyName;
//好友离线
- (void)buddyOffline:(NSString *)buddyName;
//断线
- (void)didDisconnect;
@end

@protocol XMPPMessageDelegate <NSObject>
//新消息
- (void)newMessaheReceived:(NSDictionary *)messageConnect;
@end

@interface XMPPManager : NSObject

@property (weak, nonatomic) id<XMPPConnectDelegate> connectDelegate;
@property (weak, nonatomic) id<XMPPChatDelegate> chatDelegate;
@property (weak, nonatomic) id<XMPPMessageDelegate> messageDelegate;

+ (instancetype)manager;

#pragma mark - 基本设置

//初始化xmpp
- (void)setupStream;
//登录
- (BOOL)loginWithJID:(NSString *)myJID password:(NSString *)password server:(NSString *)server;
//注册
- (BOOL)registerWithJID:(NSString *)myJID password:(NSString *)password server:(NSString *)server;
//断开连接
- (void)disconnect;

#pragma mark - 花名册

//获取好友列表
- (NSArray *)rosterList;
//添加好友
- (void)addBuddy:(NSString *)name;
//删除好友
- (void)removeBuddy:(NSString *)name;

#pragma mark - 收发消息

//发送消息
- (void)sendMessage:(NSString *)message ToUser:(NSString *)user;

@end
