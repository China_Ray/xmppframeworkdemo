//
//  UserManager.m
//  XMPPFrameworkTest
//
//  Created by Admin on 15/8/11.
//  Copyright (c) 2015年 test. All rights reserved.
//

#import "UserManager.h"

@interface UserManager ()

@end

@implementation UserManager

+ (instancetype)manager
{
    static UserManager *manager = nil;
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        manager = [[self alloc] init];
    });
    
    return manager;
}

//获取用户登录名
- (NSString *)userLoginName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"user_id"];
}

//获取用户密码
- (NSString *)userLoginPassword
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"user_password"];
}

//获取用户登录地址
- (NSString *)userLoginServer
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"user_server"];
}

//保存用户信息
- (void)saveWithName:(NSString *)_name
            Password:(NSString *)_password
              Server:(NSString *)_server
{
    self.userName     = _name;
    self.userPassword = _password;
    self.serverName   = _server;

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_name forKey:@"user_id"];
    [defaults setObject:_password forKey:@"user_password"];
    [defaults setObject:_server forKey:@"user_server"];
    [defaults synchronize];
}

@end
